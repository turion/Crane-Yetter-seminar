\documentclass[german]{enigtex-article}

\usepackage{enigtex-biblatex}
\usepackage{enigtex-cat}

\usepackage{booktabs}

\addbibresource{bib.bib}


\title{Seminar über die Crane-Yetter-TQFT und Henkelzerlegungen}
\begin{document}

\begin{abstract}
Ich werde Teile meiner vergangenen und aktuellen Forschung präsentieren.
\medskip

In meiner Doktorarbeit \cite{BaerenzThesis}
habe ich unter anderem gezeigt,
wie man mit \textbf{Henkelzerlegungen} die \textbf{Crane-Yetter-Invariante} sehr einfach definieren kann,
und zwar für beliebige Schleifenfusionskategorien (nicht nur modulare).
Die Konstruktion ist so simpel,
dass man die Invariante oft sehr schnell ausrechnen kann.
Ebenso konnte ich mit der neuen Konstruktion zeigen,
dass die Dijkgraaf-Witten-Invariante ein Spezialfall von Crane-Yetter ist,
was nicht bekannt war (da Crane-Yetter einfach zu kompliziert zu berechnen war).

Wir werden die Konstruktion im Detail anschauen und alle notwendigen Vorkenntnisse durchnehmen
(hauptsächlich Henkelzerlegungen,
ein wenig Mannigfaltigkeiten mit Ecken
und Fusionskategorien).
Dann schauen wir uns vielleicht kurz zwei Spezialfälle an und ich werde eine Vermutung über den allgemeinen Fall aufstellen.
\medskip

Ursprünglich ist Crane-Yetter als Zustandssummenmodell über Triangulierungen definiert,
was automatisch nach einem Standardrezept eine TQFT liefert.
Für Henkelzerlegungen muss man erst noch den passenden Formalismus finden,
um die Ränder der so konstruierten Mannigfaltigkeiten zu beschreiben.
Der ist zum Glück bekannt,
und nennt sich \textbf{Chirurgie-Theorie}.
Um die Zustandsräume auf den Rändern zu beschreiben,
werde ich \textbf{String-Nets} oder \textbf{Skeins} definieren,
und wir werden ein paar dieser Räume konkret ausrechnen.
Letztendlich werden wir die Crane-Yetter-Invariante zu einer TQFT erweitern.
\medskip

Als letzter, und vielleicht etwas unpräzise bleibender Schritt werde ich zeigen, wie man Crane-Yetter nochmals erweitert,
zu einer 4-2-TQFT.
Dazu müssen wir uns Mannigfaltigkeiten mit Ecken noch einmal genauer anschauen.
Man erwartet, dass Crane-Yetter sogar eine 4-0-TQFT ist,
aber wie die letzten beiden Erweiterungsschritte gehen,
weiß ich (noch) nicht.
\end{abstract}

\section{Einführung}

% TODO Allgemeines über Zustandssummenmodelle, Syntax & Semantik

\section{Vorkenntnisse}

\subsection{Henkelzerlegungen}

Die Hauptwerke sind die Bücher von Gompf \& Stipsicz,
Akbulut,
und Kirby
\cite{GompfStipsicz, Akbulut:4manifolds, Kirby:Topology4Manifolds}.

\subsubsection{Mannigfaltigkeiten mit Ecken}

Mannigfaltigkeiten mit Ecken sind ein etwas ausgespartes Thema,
ich gehe nach diesem Artikel vor:
\cite{Joyce2009ManifoldsCorners}

Hier sind die wesentlichen Definitionen und Theoreme,
die wir benötigen:

\begin{definitions}
	Im folgenden sei $M$ ein parakompakter Hausdorffraum.
	\begin{itemize}
		\item Eine \textbf{$n$-dimensionale Karte mit $k$-kodimensionalen Ecken} ist ein Tripel $(U, \phi)$,
			wobei $U \subset M$ ein offener Unterraum ist,
			und $\phi\colon U \to [0,\infty)^k \times \R^{n-k}$ eine offene Einbettung von topologischen Räumen ist.
		\item Zwei Karten $(U_1, \phi_1)$ und $(U_2, \phi_2)$ sind \textbf{kompatibel},
			falls $\phi_1 \circ \phi_2$ eingeschränkt auf die Schnittmenge glatt ist.
		\item Ein \textbf{$n$-dimensionaler Atlas mit $k$-kodimensionalen Ecken} ist eine Menge $\{(U_i, \phi_i)_{i \in I}\}$
			von $n$-dimensionalen Karten mit $k$-dimensionalen Ecken,
			die alle paarweise kompatibel sind.
			Zwei Atlanten sind \textbf{kompatibel},
			wenn ihre Vereinigung wieder ein Atlas ist.
			Es gibt \textbf{maximale Atlanten}.
		\item Eine \textbf{$n$-Mannigfaltigkeit mit $k$-kodimensionalen Ecken} ist ein parakompakter Hausdorffraum $M$ mit einem maximalen Atlas.
			Als Spezialfall haben wir \textbf{randlose Mannigfaltigkeiten} (mit 0-kodimensionalen Ecken),
			und \textbf{Mannigfaltigkeiten mit Rand} (mit 1-kodimensionalen Ecken).
	\end{itemize}
\end{definitions}
\begin{definitions}
	Sei im folgenden $M$ eine $n$-Mannigfaltigkeit mit $k$-kodimensionalen Ecken.
	\begin{itemize}
		\item Sei $(U, \phi)$ eine beliebige Karte.
			Die \textbf{Kodimension} eines Punktes $m \in U \subset M$ ist die Anzahl der ersten $k$ Koordinaten von $\phi(m)$, die gleich 0 sind.
		\item $S^i(M)$ ist der topologische Unterraum aller Punkte auf $M$ mit Kodimension $i$.
		\item Eine \textbf{lokale Randkomponente} $\beta$ eines Punktes $m \in M$
			ist eine lokale Wahl einer Zusammenhangskomponente von $S^1(M)$ in der Nähe von $m$.
			Genauer: Sei $V$ eine Umgebung von $m$ und $W \subset V \cap S^1(M)$ eine Zusammenhangskomponente,
			sodass jede Umgebung $V' \subset V$ von $m$ eine nichtleere Teilmenge mit $W$ hat,
			dann ist $W$ eine lokale Randkomponente.
			Zwei Randkomponenten $W$ und $W'$ werden identifiziert wenn $W \subset W'$.
	\end{itemize}
\end{definitions}
\begin{remarks}
	Sei $m \in M$ ein Punkt mit Kodimension $i$.
	\begin{itemize}
		\item In einer Karte $(U, \phi)$, wo $\phi(m) = (\underbrace{0,\dots,0}_i, x_{k+1}, \dots x_n)$
			sind die lokalen Randkomponenten die Umgebungen $\phi^{-1}(\{(x_1,\dots,x_{j-1},0,x_{j+1}\dots, x_n)\})$,
			mit $j \leq k$.
		\item Insbesondere hat $m$ genau $i$ lokale Randkomponenten.
	\end{itemize}
\end{remarks}
\begin{definitions}
	\begin{itemize}
		\item Der \textbf{Rand} einer Mannigfaltigkeit mit Ecken ist definiert als Menge:
			\[ \partial M \coloneqq \{(m,\beta) | m \in M, \beta \text{ lokale Randkomponente von } m \}\]
		\item Die Randimmersion $i\colon \partial M \to M$ ist definiert als $i(m, \beta) \mapsto m$.
	\end{itemize}
\end{definitions}
\begin{remarks}
	\begin{itemize}
		\item Punkte im "`Innern"' von $M$ (Punkte in $S^0(M)$) haben keine lokalen Randkomponenten
			und tauchen daher nicht im Rand auf.
		\item Für Mannigfaltigkeiten mit oder ohne Rand ist der Randbegriff äquivalent zum üblichen.
		\item Wenn $M$ Ecken von Kodimension mindestens 2 hat,
			ist $\partial M$ nicht mehr kanonisch in $M$ eingebettet.
			Ebenso gilt $\partial^2 M = \emptyset$ dann nicht mehr.
	\end{itemize}
\end{remarks}
\begin{definitions}
	\label{def:Ecken glätten}
	\begin{itemize}
		\item Es gibt für den topologischen Raum $X = [0,\infty)^2 \subset \R^2 \cong \Co$ eine eindeutige glatte Struktur,
			sodass $X$ eine Mannigfaltigkeit \emph{mit Rand} (ohne Ecken) wird.
			Zum Beispiel kann man als Karte die komplexe Abbildung $z \mapsto z^2$ nehmen und den Wertebereich auf $[0,\infty) \times \R$ einschränken.
			(Aber alle Wahlen solcher Karten sind in zwei Dimensionen äquivalent.)
			Diese glatte Struktur nennt man die \textbf{Glättung der 2-dimensionalen Ecke}
		\item Analog glättet man $[0,\infty)^2 \times \R^{n-2}$.
		\item Auf diese Weise kann man jeder Mannigfaltigkeit mit Ecken der Kodimension 2 eine Mannigfaltigkeit mit Rand zuordnen,
			indem man alle Eckenkarten im Atlas wie oben glättet.
		\item Auf ähnliche Weise stattet man die "`Ko-Ecke"' $Y = \{(x_1,x_2) | x_i \leq 0\}$,
			und Räume der Form $Y \times M$ mit einer glatten Struktur aus.
	\end{itemize}
\end{definitions}
% TODO Ecken verkleben

\subsubsection{Henkelzerlegungen}
\begin{definition}
	\item Ein \textbf{$n$-dimensionaler $k$-Henkel} ist die folgende Mannigfaltigkeit mit Ecken:
	\[ h_k \coloneqq D^k \times D^{n-k} \]
\end{definition}
\begin{remarks}
	\begin{itemize}
		\item $D^k$ ist der $k$-dimensionale Ball $\{(x_0, \dots x_k) \in \R^k | x_0^2 + \cdots + x_k^2 \leq 1 \}$
		\item Ab jetzt setzen wir $n = 4$.
		\item Ein $k$-Henkel sollte man sich als "`verdickten"' $k$-Ball vorstellen.
			Es ist eine übliche Technik in der Differentialtopologie und TQFT,
			niedrigdimensionale Objekte durch Verdickung $n$-dimensional zu machen
			(so z.B. auch bei der Definition von $G$-Strukturen auf den Rändern von Bordismen.)
		\item Glättet man einen $n$-dimensionalen Henkel,
			erhält man einen $n$-Ball.
	\end{itemize}
\end{remarks}
\begin{definition}
	Den Rand $\partial h_k = S^{k-1} \times D^{n-k} \sqcup D^k \times S^{n-k-1}$ eines $k$-Henkels wird aufgeteilt in den
	\textbf{verklebenden Rand} $\partial_{\mathbf{k}} h_k = S^{k-1} \times D^{n-k}$
	und den \textbf{verbleibenden Rand} $\partial_{\mathbf{b}} h_k = D^k \times S^{n-k-1}$.
\end{definition}
\begin{remark}
	\begin{itemize}
		\item Beispiele finden sich in Tabelle \ref{table:Henkelränder}.
		\item Ein $k$-Henkel für $1 \leq k \leq n-1$ hat Ecken der Kodimension 2.
			Daher ist sein Rand eine normale Mannigfaltigkeit mit Rand.
	\end{itemize}
\end{remark}
\begin{table}
	\begin{center}
		\begin{tabular}{llll}
			\toprule
			$k$
				& Mannigfaltigkeit mit Ecken
					& Verklebender Rand
						& Verbleibender Rand
			\\\midrule
			0
				& $D^0 \times D^4$
					& $\emptyset$
						& $S^3 \cong \R^3 \cup \{\infty\}$
			\\
			1
				& $D^1 \times D^3$
					& $S^0 \times D^3 \cong \{-1, 1\} \times D^3$
						& $D^1 \times S^2 \cong [-1, 1] \times S^2$
			\\
			2
				& $D^2 \times D^2$
					& $S^1 \times D^2$
						& $D^2 \times S^1$
			\\\bottomrule
		\end{tabular}
	\end{center}
	\caption{Die relevanten 4-dimensionalen $k$-Henkel und ihre Ränder.}
	\label{table:Henkelränder}
\end{table}
\begin{definitions}
	Sei $h_k = D^k \times D^{n-k}$ ein $k$-Henkel.
	\begin{itemize}
		\item $S^{k-1} \times \{0\} \subset \partial_\mathbf{k} h_k$ ist die \textbf{verklebende Sphäre},
		oder ``Anklebsphäre''.
		\item $\{0\} \times S^{n-k-1} \subset \partial_\mathbf{b} h_k$ ist die \textbf{verbleibende Sphäre},
		oder ``Gürtelsphäre''.
	\end{itemize}
\end{definitions}
\begin{definition}
	Ein \textbf{kompakter $k$-Henkelkörper} $H_k$ ist durch die folgende induktive Definition gegeben:
	\begin{itemize}
		\item Ein $-1$-Henkelkörper ist die leere Menge.
		\item Ein kompakter $k$-Henkelkörper ist eine Vereinigung eines $(k-1)$-Henkelkörpers mit endlich vielen $k$-Henkeln entlang der verklebenden Ränder:
			\[ H_k = (\cdots((H_{k-1} \cup_f h_k) \cup_g h_k) \cdots ) \cup h_k\]
			Dabei ist $f\colon \partial_\mathbf{k} h_k \hookrightarrow \partial H_{k-1}$ eine Einbettung des verklebenden Randes des Henkels in den Rand des Henkelkörpers.
			Die Verklebung $\cup_f$ geschieht wie folgt:
			Zunächst wird entlang $f$ identifiziert.
			Jetzt erinnern wir uns an die Ko-Ecke $Y$ aus Definition \ref{def:Ecken glätten},
			und stellen fest, dass durch die Verklebung gerade eine Ko-Ecke der Form $Y \times \partial (\partial_{\mathbf{k}}h_k)$ entsteht,
			die wir glätten können.
	\end{itemize}
	% TODO Im Prinzip ist es ja auch möglich, $k$-Henkel an $k+i$-Henkelkörper anzukleben
\end{definition}
\begin{remarks}
	\begin{itemize}
		\item Ein 0-Henkelkörper ist eine disjunkte Vereinigung von $n$-Bällen.
		\item Ein 4-dimensionaler, zusammenhängender 1-Henkelkörper ist immer diffeomorph zu einer Verklebung von endlich vielen $S^1 \times D^3$.
	\end{itemize}
\end{remarks}
\begin{definitions}
	Isotopien der Verklebeabbildungen werden \textbf{Henkelbewegungen} genannt.
	Seien $H_{k-1} \cup_f h_k$ und $H_{k-1} \cup_{f'} h_k$ also zwei Henkelkörper,
	die durch Verklebung eines einzelnen Henkels an den gleichen Unter-Henkelkörper entstanden sind.
	Dann ist eine Henkelbewegung ein Diffeomorphismus $\Phi\colon \partial H_{k-1} \to \partial H_{k-1}$ mit $f' = \Phi \circ f$,
	wobei $\Phi$ homotop zur Identität ist.
\end{definitions}
\begin{theorem}
	Eine Henkelbewegung induziert einen Diffeomorphismus der Henkelkörper.
\end{theorem}
\begin{definition}
	Eine \textbf{Henkelzerlegung} einer $n$-Mannigfaltigkeit $M$ (mit Rand) ist ein $n$-Henkelkörper $H_n$ und ein Diffeomorphismus $\phi\colon M \xrightarrow{\cong} H_n$.
\end{definition}
\begin{remarks}
	Die folgenden Aussagen gelten in allen Dimensionen.
	\begin{itemize}
		\item Man kann sich Henkelzerlegungen wie die glatte Verallgemeinerung von $CW$-Komplexen vorstellen.
		\item Glatte Mannigfaltigkeiten haben immer Henkelzerlegungen.
			Jede Morse-Funktion (mit geordneten Indizes) ergibt eine Henkelzerlegung.
		\item Kompakte Mannigfaltigkeiten haben Henkelzerlegungen mit endlich vielen Henkeln.
		\item Zusammenhängende Mannigfaltigkeiten haben Henkelzerlegungen mit genau einem 0-Henkel.
		\item Zusammenhängende, geschlossene Mannigfaltigkeiten haben Henkelzerlegungen mit genau einem 0-Henkel und genau einem $n$-Henkel.
		\item Henkelzerlegungen ohne 1-Henkel sind immer einfach zusammenhängend.
			(Die gegenteilige Behauptung ist in vier Dimensionen ein offenes Problem.)
	\end{itemize}
\end{remarks}
Das folgende Theorem ist sehr wichtig,
um unsere graphische Herangehensweise an Henkelzerlegungen zu verstehen:
\begin{theorem}
	Gegeben zwei Henkelzerlegungen zweier \emph{geschlossener} 4-Mannigfaltigkeit $M_1$ und $M_2$,
	die äquivalente 2-Henkelkörper haben.
	Dann sind $M_1$ und $M_2$ diffeomorph.
\end{theorem}
Es ist also nur notwendig, den 2-Henkelkörper einer geschlossenen 4-Mannigfaltigkeit anzugeben,
um sie bis auf Diffeomorphismus zu spezifizieren.
Da wir vorher gesehen haben, dass 1-Henkelkörper ziemlich einfache Mannigfaltigkeiten sind,
müssen wir schließen, dass das komplizierte an einer 4-Mannigfaltigkeit die 2-Henkel sind.

\subsubsection{Kirby-Diagramme}
Um zu verstehen, was für Daten eine (zusammenhängende, kompakte, orientierte) Henkelzerlegung in 4 Dimensionen spezifizieren,
schauen wir uns Tabelle \ref{table:Henkelränder} an und stellen fest,
dass wir nur wissen müssen,
wie wir 1-Henkel auf einen (zusammenhängenden) 0-Henkelkörper kleben (nämlich mittels zweier $D^3$),
und wie wir 2-Henkel auf den 1-Henkelkörper kleben (mittels einer verdickten $S^1$).
\begin{definitions}
	\begin{itemize}
		\item Ein Kirby-Diagram eines 1-Henkelkörpers ist eine Einbettung von endlich vielen Paaren von $D^3$s in $S^3 \cong \R^3 \cup \{\infty\}$,
			wobei wir per Konvention den Punkt auf Unendlich nicht berühren,
			und daher ein Diagram in $\R^3$ betrachten.
			Als Notation beschriften wir einen Ball jedes Paares mit $+$ und den anderen mit $-$.
		\item Der \emph{Rand} des Kirby-Diagrams ist $\left(S^3 \backslash \bigcup_i D_{i,+}^3 \cup D_{i,-}^3\right) / \left(S^2_{i,+} \sim \overline{S^2_{i,-}}\right)$,
			es wird also der Rand jedes $+$-Balls mit dem Rand des $-$-Balls mit umgekehrter Orientierung verklebt.
		\item Ein 2-Henkel in einem Kirby-Diagram ist eine Einbettung von $S^1 \times D^2$ in den Rand des Kirby-Diagrams.
		\item Ein (kompaktes) \textbf{Kirby-Diagram} ist ein Kirby-Diagram eines 1-Henkelkörpers und endlich viele, sich nicht überschneidende 2-Henkel.
		\item Zwei Kirby-Diagramme sind \textbf{isotop},
			wenn man das eine durch einen zur Identität homotopen Automorphismus von $S^3$ aus dem anderen erhält.
	\end{itemize}
\end{definitions}
\begin{remarks}
	\begin{itemize}
		\item Für einen 2-Henkel können wir anstatt einer Einbettung von $S^1 \times D^2$
			auch eine Einbettung von $S^1$
			(die verklebende Sphäre des 2-Henkels)
			zusammen mit einer \emph{Windungszahl} $n \in \N$ verwenden.
		\item Wenn wir ein solches Diagram regulär auf $\R^2$ projizieren
			(ohne Überschneidungen von mehr als zwei $S^1$,
			ohne Überschneidungen von $D^3$s),
			können aus der Projektion kanonisch eine Windung für jede $S^1$ festlegen
			("`Blackboard framing"').
			Z.B. hat die Standardprojektion von $S^1$ Windungzahl 0,
			und die "`Acht"' hat Windung $\pm 1$,
			je nach Orientierung der $S^1$.
		\item Ein 2-Henkel ist also entweder ein "`gerahmter Knoten"' in $S^3$,
			oder mehrere Intervalle mit Endpunkten in den $D^3$,
			so dass durch die Identifizierungen $S^2_{i,+} \sim \overline{S^2_{i,-}}$ die Intervalle zu einer $S^1$ verklebt werden.
		\item Wir können eine \emph{geschlossene} 4-Mannigfaltigkeit spezifizieren,
			indem wir ein Kirby-Diagramm für sie angeben.
			Allerdings gehört nicht jedes Kirby-Diagramm zu einer geschlossenen Mannigfaltigkeit,
			sondern unter Umständen zu einer Mannigfaltigkeit mit Rand.
			Diese ist dann wiederum nicht eindeutig,
			da sie von der Anzahl der angefügten 3-Henkel abhängt.
	\end{itemize}
\end{remarks}
Beispiele und Graphiken finden sich in \cite[Abschnitte 4.2 und 8.2]{BaerenzThesis},
sowie \cite[Kapitel 4 und 5]{GompfStipsicz} und \cite[Kapitel 1]{Akbulut:4manifolds}.

\subsubsection{Henkelbewegungen und Henkelauslöschungen}

\begin{definition}
	Ein $k$-Henkel $h_k$ und ein $(k+1)$-Henkel $h_{k+1}$ können sich \textbf{auslöschen},
	wenn die verklebende Sphäre von $h_{k+1}$ die verbleibende Sphäre (Gürtelsphäre) von $h_k$ transversal in einem Punkt schneidet.
\end{definition}
\begin{proposition}
	Entfernt man zwei sich auslöschende Henkel aus einer Henkelzerlegung,
	ist die neue Zerlegung diffeomorph zur alten.
\end{proposition}
\begin{theorem}
	Gegeben zwei Henkelzerlegungen von diffeomorphen Mannigfaltigkeiten.
	Dann gibt es eine endliche Serie von Henkelbewegungen und Henkelauslöschungen,
	die die eine Henkelzerlegung in die andere verwandelt.
\end{theorem}
\begin{remarks}
	\begin{itemize}
		\item Da wir schon wissen,
			dass Henkelbewegungen und -auslöschungen den Diffeomorphietyp nicht verändern,
			können wir mit diesem Theorem 4-Mannigfaltigkeiten einfach als Kirby-Diagramme modulo Henkelbewegungen und Henkelauslöschungen behandeln.
		\item Jede Isotopie von Kirby-Diagrammen kann als Reihe von Henkelbewegungen verstanden werden,
			aber nicht alle Henkelbewegungen sind Isotopien des Diagramms.
	\end{itemize}
\end{remarks}
\begin{examples}
	Wir wollen verstehen,
	wie Henkelbewegungen und Henkelauslöschungen in Kirby-Diagrammen aussehen.
	\begin{itemize}
		\item Eine Henkelbewegung von einem 1-Henkel (auf dem 0-Henkel) ist gerade eine Isotopie der beiden $D^3$s im Kirby-Diagramm,
			die die Identifizierung ihrer Ränder erhält.
		\item Bei einer Henkelbewegung von einem 1-Henkel über einen weiteren 1-Henkel verschiebt man eine $D^3$ des bewegten Henkels ``durch'' eine $D^3$ des weiteren Henkels,
			wobei sie bei der anderen $D^3$ wieder herauskommt.
			Angeklebte 2-Henkel werden ``hinterhergezogen''.
			Eine Abbildung findet man z.B. in \cite[Abbildung 5.2]{GompfStipsicz}.
		\item Eine Henkelbewegung von einem 2-Henkel auf dem 0-Henkelkörper ist eine Isotopie der verklebenden $S^1$ im Kirby-Diagramm.
		\item Eine Henkelbewegung von 2-Henkeln über einen anderen 2-Henkel lässt sich durch den folgenden Prozess darstellen:
			Zunächst verdoppelt man den anderen 2-Henkel entlang seiner Rahmung.
			Dann wählt man auf verklebenden $S^1$ des bewegenden und des verdoppelten Henkels jeweils ein Intervall,
			und eine Isotopie der Intervalle im Kirby-Diagramm.
			Dann ersetzt man die die verklebende $S^1$ durch ihre zusammenhängende Summe mit der verdoppelten $S^1$,
			entlang des Randes der Isotopie.
			Dabei wird die Rahmung des anderen 2-Henkels zu der des bewegenden Henkels addiert.
			
			Darstellungen davon findet man z.B. in \cite[Abbildungen 5.4 bis 5.11]{GompfStipsicz} und \cite[Kapitel 1.2]{Akbulut:4manifolds}.
		\item Henkelauslöschungen von 0- und 1-Henkeln müssen wir nicht betrachten,
			weil wir immer annehmen,
			dass wir nur einen 0-Henkel haben.
		\item Henkelauslöschungen von 1- und 2-Henkeln können dann stattfinden,
			wenn die verklebende $S^1$ vom 2-Henkel die verbleibende "`Gürtel"'-$S^2$ des 1-Henkels transversal in einem Punkt schneidet.
			Die $S^2$ befindet sich im Innern des 1-Henkels,
			das wir nicht zeichnen.
			Da wir aber annehmen,
			dass die $S^1$ im Innern genauso wie auf den Rändern der beiden $D^3_\pm$ des 1-Henkels verläuft,
			ist es für die Auslöschbarkeit notwendig und hinreichend,
			wenn die $S^1$ den Rand der $D^3_+$ (und damit auch der $D^3_-$) transversal in einem Punkt schneidet.
			% TODO Referenz in DrArbeit und GompfStipsicz
			
			Falls der 2-Henkel noch über andere 1-Henkel verläuft,
			bewegen wir die 1-Henkel erst übereinander,
			bis der 2-Henkel nur noch über den 1-Henkel verläuft,
			mit dem er sich auslöscht.
			% TODO Referenzbild
			
			Die Auslöschung sieht dann so aus,
			dass man die beiden $D^3_\pm$ entlang des sichtbaren Intervalls der $S^1$ aufeinander zubewegt und dabei alle etwaigen weiteren 2-Henkel auf dem 1-Henkel hinterherzieht.
			Sobald das Intervall verschwindet,
			entfernt man die $D^3_\pm$ und verbindet die übrigen 2-Henkel.
		\item Eine 2-3-Henkelauslöschung ist sehr einfach zu zeichnen.
			Da die 3-Henkel im Falle von geschlossenen Mannigfaltigkeiten nicht notiert werden müssen,
			brauchen wir nur zu erkennen,
			welche 2-Henkel mit einem 3-Henkel auslöschbar sind.
			Das sind gerade die ungerahmten (unverdrehten) Unknoten,
			also die trivialen, zusammenziehbaren Einbettungen.
			Die kann man also beliebig dem Diagramm hinzufügen oder davon entfernen.
	\end{itemize}
\end{examples}

\subsection{Fusionskategorien}

Wir müssen die Begriffe aus Tabelle \ref{table:Grundbegriffe} verstehen.
\begin{table}
	\begin{tabular}{lll}
		\toprule
		Begriff
			& Englischer Begriff
				& Notation
		\\
		\midrule
		Sphärische Fusionskategorien
			& spherical fusion categories
				& \shortstack{$\mathcal{C}, \mathcal{D}\dots$
				\\$\tr\colon \mathcal{C}(X,X) \to \mathcal{C}(I,I)$
				\\$\left<f,g\right> = \tr(fg)$}
		\\
		\midrule
		Zopfstrukturen
			& braidings
				& $c_{X,Y}$
		\\
		\midrule
		Schleifenfusionskategorien
			& ribbon fusion categories
				& $\mathcal{C}, \mathcal{D}\dots$
		\\
		\midrule
		Symmetrische Fusionskategorien
			& symmetric fusion categories
				& $\mathcal{C}, \mathcal{D}\dots \quad c_{X,Y} = c_{Y,X}^{-1}$
		\\
		\midrule
		Drinfeld-Zentrum
			& Drinfeld centre
				& $\mathcal{Z}(\mathcal{C})$
		\\
		\midrule
		Einfache Objekte
			& Simple objects
				& $X \in \Lambda_\mathcal{C}$
		\\
		\midrule
		Kirby-Farben
		(reguläre Darstellungen)
			& Kirby colours
				& $\Omega_\mathcal{C} \coloneqq \bigoplus_{X \in \Lambda_\mathcal{C}} \qdim{X} X$
		\\
		\midrule
		Gauss-Summen
		& Gauss sums
		& $\Omega^\pm_\mathcal{C} \coloneqq \bigoplus_{X \in \Lambda_\mathcal{C}} \tr(\theta_X^{\pm 1}) X$
		\\
		\midrule
		Symmetrisches Zentrum ("`Müger-Zentrum"')
			& symmetric centre
				& $\mathcal{C}'$
		\\
		\bottomrule
	\end{tabular}
	\caption{Grundbegriffe um die algebraische Seite von Crane-Yetter zu verstehen}
	\label{table:Grundbegriffe}
\end{table}

Diese Begriffe sind z.B. auf Englisch in meiner Doktorarbeit \cite[Kapitel 2 und 4.1]{BaerenzThesis} erklärt.
Ich verwende auch die Konventionen von dort.

\section{Die Crane-Yetter-Invariante}

Im wesentlichen folgen wir \cite[Kapitel 5.2 und 6.3]{BaerenzThesis},
allerdings etwas vereinfacht,
da wir nur den Spezialfall der Crane-Yetter-Invariante betrachten.


\begin{definition}
	Ein \textbf{eingebetteter Schleifengraph} in einer 3-dimensionalen Mannigfaltigkeit $M$ ist gegeben durch folgende Daten:
	\begin{itemize}
		\item Ein Graph $(V,E)$,
		\item eine injektive Abbildung $p\colon V\colon M$ der Vertizes in die Mannigfaltigkeit,
		\item für jede Kante $e\colon v_0 \to v_1$ ein eingebettetes, gerahmtes Intervall $i\colon [0,1] \to M$,
			sodass $i(k) = p(v_k)$ ist
			(also das Intervall zwischen den Einbettungen der Vertizes verläuft).
			``Gerahmt'' bedeutet hier, dass ein Schnitt des Normalenbündels des Intervalls gegeben ist.
	\end{itemize}
\end{definition}
\begin{definition}
	Sei $\pi\colon \R^3 \twoheadrightarrow \R^2$ die Projektion auf die ersten beiden Komponenten.
	Ein Schleifengraph ist regulär in $\R^3$ eingebettet, wenn folgende Eigenschaften gelten (und vermutlich noch ein oder zwei, die ich vergessen habe, die aber nicht essentiell sind):
	\begin{itemize}
		\item An jedem Punkt in $\R^2$ befinden sich höchstens zwei Kanten
		(es können in der Projektion sich nicht mehr als zwei Kanten schneiden).
		\item Die Einbettung der Vertizes ist in der Projektion immer noch injektiv.
		\item Die Einbettungen der Kanten werden zu Immersionen.
		\item Die Rahmung der Kanten kommt von der ``Tafelrahmung'' (black board framing).
	\end{itemize}
\end{definition}
\begin{lemma}
	Es ist immer möglich einen in $\R^3$ eingebetteten Schleifengraph durch zur Identität isotopen Automorphismen von $\R^3$ zu ``regularisieren''.
\end{lemma}

\begin{definition}[Graphisches Kalkül, siehe \cite{Shum:1994TortileTensorCats, Selinger:graphical}]
	Gegeben die folgenden Daten:
	\begin{itemize}
		\item $\mathcal{C}$ eine Schleifenfusionskategorie,
		\item $G$ ein eingebetteter Schleifengraph,
			sodass Kanten in Vertizes in der Projektion immer "`von oben"' oder "`von unten"' kommen.
		\item $X\colon E \to \ob \mathcal{C}$ eine Abbildung von den Kanten in die Objekte von $\mathcal{C}$ (die ``Objektdekoration''),
		\item $\iota\colon V \to \mor \mathcal{C}$ eine Abbildung von den Vertizes in die Morphismen (die ``Morphismendekoration'').
	\end{itemize}
	Wir definieren die folgenden Begriffe:
	\begin{itemize}
		\item Sei $v$ ein Vertex von $G$,
			sodass in der Projektion von links nach rechts die Kanten $e_1\dots e_n$ von oben reingehen bzw. rauskommen,
			und die Kanten $e_1'\dots e_m'$.
			Wir definieren $X_i = X(e_i)$ für reingehende Kanten,
			und $X_i = X(e_i)^*$ für rauskommende.
			$\iota$ ist auf $v$ \textbf{wohltypisiert}, ``kompatibel'' oder ``zulässig'',
			falls $\iota(v)\colon X_1 \otimes X_2 \cdots X_n \to X_1' \otimes X_2' \cdots X_n'$.
		\item Sei $\iota$ eine wohltypisierte Morphismendekoration.
			Die \textbf{Evaluation} von $L$ mit gegebener Objektdekoration $X$ ist der Endomorphismus $\left<G(X,\iota)\right> \colon I \to I$,
			den man aus folgendem Diagramm in $\mathcal{C}$ erhält:
			\begin{itemize}
				\item Für die Vertizes $v$ setzt man $\iota(v)$ ein.
				\item Für vertikale Linien jeder Kante $e$ setzt man $1_{X(i)}$ ein,
					falls die Linie aufwärts gerichtet ist, ansonsten das Dual davon.
				\item Für Maxima und Minima setzt man die Einheiten und Koeinheiten der Dualität ein.
				\item Für Überkreuzungen oder Verzopfungen setzt man die Zopfstruktur von $\mathcal{C}$ ein.
			\end{itemize}
	\end{itemize}
\end{definition}
\begin{remark}
	Kirby-Diagramme sind im wesentlichen eine spezielle Sorte Schleifengraph,
	daher können wir sie im diagrammatischen Kalkül von $\mathcal{C}$ auf die oben beschriebene Weise auswerten.
	Als Anforderungen an ein Kirby-Diagramm $L$, (mit einer Wahl der Projektion in die Ebene) brauchen wir,
	dass bei jedem +-Ball alle 2-Henkel in der Ebene "`von oben"' kommen,
	und bei jedem $-$-Ball von unten.
	Nun interpretieren wir die Bälle der 1-Henkel als Vertizes,
	und die verschiedenen Abschnitte der 2-Henkel als Kanten.
	
	Dann erhalten wir als Objekt- und Morphismendekoration:
	\begin{itemize}
		\item $X\colon L_2 \to \ob \mathcal{C}$ eine Abbildung von den 2-Henkeln\footnote{Wir nehmen im weiteren an, dass alle Abschnitte des 2-Henkels mit demselben Objekt dekoriert werden.}
			von $L$ in die Objekte von $\mathcal{C}$.
		\item $\iota_\pm\colon L_1 \to \mor \mathcal{C}$ eine Abbildung von den $\pm$-Bällen der 1-Henkel in die Morphismen.
	\end{itemize}
	$\iota(h_1)\colon X_1 \otimes X_2 \cdots X_n \to I$ ist nun wohltypisiert,
	wenn in den zugehörigen +-Ball von $h_1$ in der Projektion von links nach rechts die 2-Henkel $h_{2,1}\dots h_{2,n}$ reingehen bzw. rauskommen,
	und $X(h_{2,i}) = X_i$ für reingehende 2-Henkel,
	bzw. $X(h_{2,i})^* = X_i$ für rauskommende.
\end{remark}
\begin{definition}
	Sei $L$ ein Kirby-Diagram einer geschlossenen, glatten, orientierten 4-Mannigfaltigkeit $M$.
	\begin{itemize}
		\item Eine \textbf{Basiswahl} für $X$ ist,
		in der vorherigen Notation,
		die Wahl einer Basis für jeden der Räume $\mathcal{C}(X_1 \otimes X_2 \cdots X_n, I)$,
		und die gleichzeitige Wahl der dualen Basis auf $\mathcal{C}(I, X_1 \otimes X_2 \cdots X_n)$,
		bezüglich der sphärischen Paarung.
		\item Für eine gegebene Objektdekoration $X$ betrachten wir nur wohltypisierte Morphismendekorationen $\iota$
		sodass für jeden 1-Henkel $h_{1,i}$ der Morphismus $\iota_+(h_{1,i})$ ein Basisvektor ist und $\iota_-(h_{1,i})$ der dazugehörige duale Basisvektor ist.
		\item Die \textbf{Crane-Yetter-Invariante} $\widehat{CY}_\mathcal{C}(L)$ ist definiert als:
		\begin{equation}
			\widehat{CY}_\mathcal{C}(L) \coloneqq
			\qdim{\Omega_\mathcal{C}}^{\lvert L_1 \rvert - \lvert L_2 \rvert}
			\sum_{X, \iota} \left<L(X, \iota)\right>
			\prod_{h_2 } \qdim{X(h_2)}
		\end{equation}
		Dabei sind $X$ Objektdekorationen der 2-Henkel (und implizite Basiswahlen),
		$\iota$ wohltypisierte Morphismendekorationen mit Werten in den Basen,
		und $h_2$ alle 2-Henkel.
	\end{itemize}
\end{definition}
\begin{theorem}
	\begin{itemize}
		\item $\widehat{CY}_\mathcal{C}$ hängt nicht von der Wahl von $L$ ab und ist daher eine Invariante von Mannigfaltigkeiten.
			(Konsequenterweise schreiben wir dann auch $\widehat{CY}_\mathcal{C}(M)$.)
		\item $\widehat{CY}_\mathcal{C}(M) = CY_\mathcal{C}(M) \cdot \qdim{\Omega_\mathcal{C}}^{1 - \chi(M)}$, wobei $CY$ die ursprüngliche Crane-Yetter Zustands\-summe \cite{CraneYetterKauffman:1997177} und $\chi$ die Euler-Charakteristik sind.
	\end{itemize}
\end{theorem}
\begin{proof}
Siehe Tafel oder \cite[Theorem 5.8, Proposition 8.1]{BaerenzThesis}.
\end{proof}
Beispielrechnungen finden sich in \cite[Section 8]{BaerenzThesis}.
Die wesentlichen Erkenntnisse sind,
für eine Mannigfaltigkeit $M$ und eine Schleifenfusionskategorie $\mathcal{C}$:
\begin{itemize}
	\item Für einfach zusammenhängende $M$, oder für modulare $\mathcal{C}$,
		ist $CY$ vollständig durch die Signatur und Euler-Charakteristik von $M$,
		und durch die Gauss-Summen von $\mathcal{C}$ gegeben.
		(Insbesondere ist $CY$ für modulare $\mathcal{C}$ invertierbar,
		was schon seit 20 Jahren bekannt ist.)
	\item Für nicht modulare $\mathcal{C}$ ist Crane-Yetter abhängig von der Fundamentalgruppe von $M$,
		und vom symmetrischen Zentrum von $\mathcal{C}$.
		Insbesondere ist $CY$ mindestens so stark wie die Dijkgraaf-Witten Invariante mit trivialem Kozykel,
	\footnote{Sei $G$ eine endliche Gruppe.
		Dann ist $DW_G(M) = \lvert \{ \phi\colon \pi_1(M) \to G \}\rvert$,
		also die Anzahl der Homomorphismen von der Fundamentalgruppe nach $G$,
		oder auch die Anzahl der $G$-Bündel.}
	denn für eine endliche Gruppe $G$ und $\mathcal{C} = \Rep(G)$ (die endlichen Darstellungen von $G$) ist gerade $\widehat{CY}_\mathcal{C} = DW_G$.
\end{itemize}
Fazit: Crane-Yetter ist stärker als nur Homologie,
aber es ist nicht bekannt, wie stark genau.
Allerdings gibt es die folgende Vermutung:
\begin{conjecture}
	Sei $\mathcal{C}$ eine Schleifenfusionskategorie,
	$\mathcal{C}'$ das symmetrische Zentrum,
	und $\widetilde{\mathcal{C}}$ die Modularisierung\footnote{Die Modularisierung ist die universelle modulare Kategorie zusammen mit einem Funktor $H\colon \mathcal{C} \to \widetilde{\mathcal{C}}$ sodass die Verkettung $\mathcal{C}' \hookrightarrow \mathcal{C} \xrightarrow{H} \widetilde{\mathcal{C}}$ alle Objekte auf Vielfache von $I$ abbildet.
		Sie ist eine Verallgemeinerung von ``Faserfunktoren'' $\Rep_G \to \Vect$,
		da eine symmetrische Fusionskategorie unter milden Annahmen immer äquivalent zu $\Rep_G$ für eine endliche Gruppe $G$ ist.}.
	Dann gilt:
	\begin{equation}
		\widehat{CY}_\mathcal{C} = \widehat{CY}_{\mathcal{C}'} \cdot \widehat{CY}_{\widetilde{\mathcal{C}}}
	\end{equation}
\end{conjecture}
\section{Chirurgietheorie und relative Henkelzerlegungen}

% TODO 3-Henkel notwendig um alle Mannigfaltigkeiten zu kriegen? Oder machen die den Rand unzusammenhängend? 4-Henkel? Siehe Kirby?

\printbibliography

\end{document}
